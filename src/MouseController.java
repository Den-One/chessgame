import pieceFigures.PieceObjects;
import pieceFigures.PieceRefactored;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class MouseController {

    private PieceRefactored selectedPiece = null;

    public MouseController(WindowFrame windowFrame, PieceObjects objects) {
        JFrame frame = windowFrame.getFrame();
        frame.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (selectedPiece != null) {
                    selectedPiece.setX(e.getX() - 32);
                    selectedPiece.setY(e.getY() - 32);
                    frame.repaint();
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });

        frame.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                //System.out.println((getPiece(e.getX(), e.getY()).isWhite ?"white ":"black ") + getPiece(e.getX(), e.getY()).name);
                selectedPiece = objects.findPieceOn(e.getX(), e.getY());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                selectedPiece.move(selectedPiece.getName(), e.getX() / 64, e.getY() / 64);
                frame.repaint();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }
}