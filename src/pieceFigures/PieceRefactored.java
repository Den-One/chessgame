package pieceFigures;

public class PieceRefactored extends PieceObjects
        implements Bishop, King, Knight, Pawn, Queen, Rook {

    private int numberOnBoardX;
    private int numberOnBoardY;
    private int x;
    private int y;
    private boolean isWhite;
    private String name;
    private int index;

    final int IMG_HEIGHT = 64;
    final int IMG_WIDTH = 64;

    public PieceRefactored(int numberOnBoardX, int numberOnBoardY, boolean isWhite, String name, int index) {

        this.numberOnBoardX = numberOnBoardX;
        this.numberOnBoardY = numberOnBoardY;
        this.x = numberOnBoardX * IMG_WIDTH;
        this.y = numberOnBoardY * IMG_HEIGHT;
        this.isWhite = isWhite;
        this.name = name;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getName() {
        return name;
    }

    @Override
    public void move(String name, int destinationX, int destinationY) {
        switch (name) {
            case "ROOK_1":
            case "ROOK_2":
                rookMove(name, destinationX, destinationY); break;
            case "KNIGHT_1":
            case "KNIGHT_2":
                knightMove(name, destinationX, destinationY); break;
            case "BISHOP_1":
            case "BISHOP_2":
                bishopMove(name, destinationX, destinationY); break;
            case "KING":
                kingMove(name, destinationX, destinationY); break;
            case "QUEEN":
                queenMove(name, destinationX, destinationY); break;
            case "PAWN_1":
            case "PAWN_2":
            case "PAWN_3":
            case "PAWN_4":
            case "PAWN_5":
            case "PAWN_6":
            case "PAWN_7":
            case "PAWN_8":
                pawnMove(name, destinationX, destinationY); break;
        }
    }

    private void rookMove(String name, int destinationX, int destinationY)
    {
        if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT) != null)
        {
            if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).isWhite != isWhite)  {
             findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).kill();
            }
            else {
                x = this.numberOnBoardX * IMG_WIDTH;
                y = this.numberOnBoardY * IMG_HEIGHT;
                return;
            }

            this.numberOnBoardX = destinationX;
            this.numberOnBoardY = destinationY;
            x = destinationX * IMG_WIDTH;
            y = destinationY * IMG_HEIGHT;

        }
    }

    private void knightMove(String name, int destinationX, int destinationY)
    {
        if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT) != null)
        {
            if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).isWhite != isWhite)  {
                findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).kill();
            }
            else {
                x = this.numberOnBoardX * IMG_WIDTH;
                y = this.numberOnBoardY * IMG_HEIGHT;
                return;
            }

            this.numberOnBoardX = destinationX;
            this.numberOnBoardY = destinationY;
            x = destinationX * IMG_WIDTH;
            y = destinationY * IMG_HEIGHT;

        }
    }

    private void bishopMove(String name, int destinationX, int destinationY)
    {
        if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT) != null)
        {
            if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).isWhite != isWhite)  {
                findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).kill();
            }
            else {
                x = this.numberOnBoardX * IMG_WIDTH;
                y = this.numberOnBoardY * IMG_HEIGHT;
                return;
            }

            this.numberOnBoardX = destinationX;
            this.numberOnBoardY = destinationY;
            x = destinationX * IMG_WIDTH;
            y = destinationY * IMG_HEIGHT;

        }
    }

    private void kingMove(String name, int destinationX, int destinationY)
    {
        if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT) != null)
        {
            if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).isWhite != isWhite)  {
                findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).kill();
            }
            else {
                x = this.numberOnBoardX * IMG_WIDTH;
                y = this.numberOnBoardY * IMG_HEIGHT;
                return;
            }

            this.numberOnBoardX = destinationX;
            this.numberOnBoardY = destinationY;
            x = destinationX * IMG_WIDTH;
            y = destinationY * IMG_HEIGHT;

        }
    }

    private void queenMove(String name, int destinationX, int destinationY)
    {
        if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT) != null)
        {
            if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).isWhite != isWhite)  {
                findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).kill();
            }
            else {
                x = this.numberOnBoardX * IMG_WIDTH;
                y = this.numberOnBoardY * IMG_HEIGHT;
                return;
            }

            this.numberOnBoardX = destinationX;
            this.numberOnBoardY = destinationY;
            x = destinationX * IMG_WIDTH;
            y = destinationY * IMG_HEIGHT;

        }
    }

    private void pawnMove(String name, int destinationX, int destinationY)
    {
        if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT) != null)
        {
            if (findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).isWhite != isWhite)  {
                findPieceOn(destinationX * IMG_WIDTH, destinationY * IMG_HEIGHT).kill();
            }
            else {
                x = this.numberOnBoardX * IMG_WIDTH;
                y = this.numberOnBoardY * IMG_HEIGHT;
                return;
            }

            this.numberOnBoardX = destinationX;
            this.numberOnBoardY = destinationY;
            x = destinationX * IMG_WIDTH;
            y = destinationY * IMG_HEIGHT;

        }
    }
}