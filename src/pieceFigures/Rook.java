package pieceFigures;

public interface Rook {
    public void move(String name, int destinationX, int destinationY);
}