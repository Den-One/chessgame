package pieceFigures;

public interface King {
    public void move(String name, int destinationX, int destinationY);
}