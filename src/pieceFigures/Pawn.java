package pieceFigures;

public interface Pawn {
    public void move(String name, int destinationX, int destinationY);
}
