package pieceFigures;

public interface Bishop {
    public void move(String name, int destinationX, int destinationY);
}
