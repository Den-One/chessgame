package pieceFigures;

import pieceFigures.*;

import java.util.LinkedList;
import java.util.Objects;

public class PieceObjects {
    LinkedList<PieceRefactored> pieces = new LinkedList<PieceRefactored>();

    public LinkedList<PieceRefactored> getPieces() {
        return pieces;
    }

    public PieceObjects() {

        createPieces("BLACK");
        createPieces("WHITE");
    }

    private void createPieces(String color) {

        boolean currentColorBlack;
        int curPosYPawn = 1;
        int curPosYOther = 0;

        currentColorBlack = !Objects.equals(color, "BLACK");
        if (!currentColorBlack) {
            curPosYOther += 7;
            curPosYPawn += 5;
        }

        for (int i = 0; i < 8; i++) {
            pieces.add(new PieceRefactored(i, curPosYOther, currentColorBlack, PieceNames.PIECE_INDEXED[i].name(), PieceNames.PIECE_INDEXED[i].pieceIndex));
        }

        for (int i = 0; i < 8; i++) {
            pieces.add(new PieceRefactored(i, curPosYPawn, currentColorBlack, PieceNames.PIECE_INDEXED[i].name(), PieceNames.PIECE_INDEXED[i].pieceIndex));
        }
    }

    public PieceRefactored findPieceOn(int x, int y) {
        if (pieces.isEmpty()) {
            throw new NullPointerException("Pieces wasn't add to the List");
        }
        else {
            for (PieceRefactored piece : pieces) {
                if (piece.getX() == x && piece.getY() == y) {
                    return piece;
                }
            }
            throw new NullPointerException("Pieces wasn't found in the List");
        }
    }

    public void kill(){
        pieces.remove(this);
    }
}