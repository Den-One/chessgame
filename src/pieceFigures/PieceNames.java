package pieceFigures;

public enum PieceNames {

    ROOK_1(4),
    KNIGHT_1(3),
    BISHOP_1(2),
    QUEEN(1),
    KING(0),
    BISHOP_2(2),
    KNIGHT_2(3),
    ROOK_2(3),

    PAWN_1(5),
    PAWN_2(5),
    PAWN_3(5),
    PAWN_4(5),
    PAWN_5(5),
    PAWN_6(5),
    PAWN_7(5),
    PAWN_8(5);

    int pieceIndex;

    PieceNames(int id) {
        this.pieceIndex = id;
    }

    public static PieceNames[] PIECE_INDEXED =
            new PieceNames[] { ROOK_1, KNIGHT_1,BISHOP_1, QUEEN,
                    KING, BISHOP_2, KNIGHT_2, ROOK_2,

                    PAWN_1, PAWN_2, PAWN_3, PAWN_4,
                    PAWN_5, PAWN_6, PAWN_7, PAWN_8 };
}
