package pieceFigures;

public interface Knight {
    public void move(String name, int destinationX, int destinationY);
}
