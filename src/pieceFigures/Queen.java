package pieceFigures;

public interface Queen {
    public void move(String name, int destinationX, int destinationY);
}
