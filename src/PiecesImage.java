import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PiecesImage {

    private final BufferedImage image;
    private Image[] imageParts;

    private final int HEIGHT;
    private final int WIDTH;

    public PiecesImage(String fileName) throws IOException {
        image = ImageIO.read(new File(fileName));
        HEIGHT = image.getHeight();
        WIDTH = image.getWidth();
    }

    public void divideImageOn(int pieces) {
        imageParts = new Image[pieces];
        
        int i = 0; // 128x384
        for (int y = 0; y < HEIGHT; y += HEIGHT / 2) {
            for (int x = 0; x < WIDTH; x += WIDTH / 6, ++i) {
                imageParts[i] = image.getSubimage(x, y, WIDTH / 6, HEIGHT / 2);
            }
        }
    }

    public Image getImagePart(int i) {
        if (imageParts.length > 0) {
            return imageParts[i];
        }
        else {
            throw new NullPointerException("Image wasn't divided");
        }
    }
}