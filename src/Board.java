import pieceFigures.PieceObjects;
import pieceFigures.PieceRefactored;

import javax.swing.*;
import java.awt.*;

public class Board {
    private JPanel panel;

    public Board (PiecesImage image, PieceObjects objects) {
        panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                boolean white = true;
                for (int y = 0; y < 8 ; y++) {
                    for(int x= 0;x<8;x++){
                        if(white){
                            g.setColor(new Color(3, 30, 83));
                        }else{
                            g.setColor(new Color(0, 5, 20));

                        }
                        g.fillRect(x*64, y*64, 64, 64);
                        white=!white;
                    }
                    white=!white;
                }

                for (PieceRefactored p: objects.getPieces()) {
                    g.drawImage(image.getImagePart(p.getIndex()), p.getX(), p.getY(), this);
                }
            }
        };
    }

    public JPanel getBoard() {
        return panel;
    }
}