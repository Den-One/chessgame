import javax.swing.*;

public class WindowFrame {

    private JFrame frame;

    public WindowFrame (Board board) {
        frame = new JFrame();
        frame.setBounds(10, 10, 512, 512);
        frame.setUndecorated(true);

        frame.add(board.getBoard());
        frame.setDefaultCloseOperation(3);
        frame.setVisible(true);
    }

    public JFrame getFrame() {
        return frame;
    }
}