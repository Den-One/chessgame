import pieceFigures.PieceObjects;

import java.io.IOException;

public class ChessGameRef {
    public static void main(String[] args) throws IOException {

        PiecesImage piecesImage = new PiecesImage("C:\\Users\\pcuser\\IdeaProjects\\ChessGame\\pictures\\128x384_BlackAndWhite_TileSheet.png");
        piecesImage.divideImageOn(12);
        PieceObjects pieceObjects = new PieceObjects(); // piecesImage
        Board panel = new Board(piecesImage, pieceObjects);
        WindowFrame window = new WindowFrame(panel);
        //MouseController mouseController = new MouseController(window, pieceObjects);
    }
}